import math
import pytest

def quadratic_solution (a,b,c):
    if a==0:
        print('Enter a non zero first coefficient.')
    else: 
        D= b**2-4*a*c
        if D>0:
            X1= (-b-math.sqrt(D))/(2*a)
            X2= (-b+math.sqrt(D))/(2*a)
            print('There are 2 solutions:')
            print(" Ther are two solutions X1 = ",X1, "and X2 = ", X2)
            return X1, X2

        elif D==0:
            X= -b/(2*a)
            print ('There is a one solution:')
            print (X)
            return X
    
        else:
            raise ValueError ('There is no real solution.')
    return 
#This section is to test if the function works
def test_quadratic_solution ():
    assert quadratic_solution(1, 0, 0) == 0 

def test_quadratic_double_solution():
    pass

def test_quadratic_no_solution():
    with pytest.raises(ValueError):
        quadratic_solution(1, 2, 3)